import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { PropertyAddingService } from './property-adding.service';
import { Property } from '../common/interfaces/property.interface';

@Component({
  selector: 'app-property-adding',
  templateUrl: './property-adding.component.html',
  styleUrls: ['./property-adding.component.scss']
})
export class PropertyAddingComponent implements OnInit {

  public config: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 10,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };

  isLinear = false;
  basicInfo: FormGroup;
  picturesAndPrices: FormGroup;
  facilities: FormGroup;
  formalities: FormGroup;
  selectRoomQtyValue = null;

  selectRoomQty;
  selectedRooms = new Array();

  selectedValue = '1';

  get selected() {
    return this.selectedValue;
  }
  set selected(val: string) {
    const roomCount = parseInt(val, 10);
    this.selectedValue = val;
    this.selectedRooms = new Array(roomCount);
    this.basicInfo.removeControl('items');
    this.basicInfo.addControl('items', this._formBuilder.array([]));
    for (let i = 0; i < roomCount; i++) {
      this.addItem();
    }
  }

  constructor(private propertiesAddingService: PropertyAddingService, private _formBuilder: FormBuilder) {}

  ngOnInit() {

    this.basicInfo = this._formBuilder.group({
      propertyName: ['', Validators.required],
      propertyOwnerName: ['', Validators.required],
      propertyOwnerEmail: ['', Validators.required],
      propertyOwnerPhone: ['', Validators.required],
      propertySubtitle: [''],
      city: ['', Validators.required],
      street: ['', Validators.required],
      postalCode: ['', Validators.required],
      selectRoomQty: ['0', Validators.required],
      rooms: this._formBuilder.array([]),
      minDaysCondition: [''],
      minDays: [''],
      higherPriceCondition: [''],
      higherPriceDays: [''],
      higherPrice: [''],
      lowerPriceCondition: [''],
      lowerPriceDays: [''],
      lowerPrice: ['']
    });

    this.picturesAndPrices = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    this.facilities = this._formBuilder.group({
      wifi: [''],
      shampoo: [''],
      towels: [''],
      bedding: [''],
      soap: [''],
      toiletPaper: [''],
      TV: [''],
      heating: [''],
      wardrobes: [''],
      airConditioning: [''],
      breakfast: [''],
      desk: [''],
      firePlace: [''],
      iron: [''],
      fridge: [''],
      kettle: [''],
      hairDryer: [''],
      livingRoom: [''],
      swimmingPool: [''],
      kidsRoom: [''],
      kitchen: [''],
      loundry: [''],
      parking: [''],
      elevator: [''],
      jacuzzi: [''],
      gym: [''],
      playground: [''],
    });

    this.formalities = this._formBuilder.group({
      propertyDescription: [''],
      propertyDetails: [''],
      propertyLocalization: [''],
      bankAccountName: ['', Validators.required],
      bankAccountNumber: ['', Validators.required],
      prepaymentAmount: ['', Validators.required],
      prepaymentTerm: ['', Validators.required],
    });
  }

  createItem(): FormGroup {
    return this._formBuilder.group({
      name: '',
      price: '',
      description: ''
    });
  }

  addItem(): void {
    const items = this.basicInfo.get('items') as FormArray;
    items.push(this.createItem());
  }
  onUploadError(args: any) {
  console.log('onUploadError:', args);
  }
  onUploadSuccess(args: any) {
  console.log('onUploadSuccess:', args);
  }

  onSubmit() {
    if (this.facilities.valid){
      console.log("Form facilities submitted", this.facilities.value)
    }
    if (this.formalities.valid){
      console.log("Form formalities submitted", this.formalities.value)
    }
    this.propertiesAddingService.addProperty({
      id: '1',
      owner_name: this.basicInfo.value.propertyOwnerName,
      owner_email: this.basicInfo.value.propertyOwnerEmail,
      owner_phone: this.basicInfo.value.propertyOwnerPhone,
      name: this.basicInfo.value.propertyName,
      name_subtitle: this.basicInfo.value.propertySubtitle,
      city: this.basicInfo.value.city,
      street: this.basicInfo.value.street,
      postalCode: this.basicInfo.value.postalCode,
      images: [],
      amenities: [],
      offer_description: this.formalities.value.propertyDescription,
      offer_description_details: this.formalities.value.propertyDetails,
      bankAccountName: this.formalities.value.bankAccountName,
      bankAccountNumber: this.formalities.value.bankAccountNumber,
      prepaymentAmount: this.formalities.value.prepaymentAmount,
      prepaymentTerm: this.formalities.value.prepaymentTerm,
      logo: 'blaa'
    }).subscribe(() => {

    });
  }

}
