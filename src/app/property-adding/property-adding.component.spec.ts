import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyAddingComponent } from './property-adding.component';

describe('PropertyAddingComponent', () => {
  let component: PropertyAddingComponent;
  let fixture: ComponentFixture<PropertyAddingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyAddingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyAddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
