import {Injectable} from '@angular/core';
import {HttpService} from '../common/services/http.service';
import {Property} from '../common/interfaces/property.interface';

@Injectable({
  providedIn: 'root'
})
export class PropertyAddingService {

  constructor(private httpService: HttpService) {
  }

  addProperty(data: Property) {
    return this.httpService.post('properties', data);
  }
}
