import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './common/services/http.service';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HeaderComponent } from './header/header.component';
import { MatListModule, MatCheckboxModule } from '@angular/material';
import { MatButtonModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatStepperModule } from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PropertyAddingComponent } from './property-adding/property-adding.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material';
import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { CommonModule } from '@angular/common';
import { MatSliderModule} from '@angular/material';
import { MatSlideToggleModule} from '@angular/material';
import { MatIconModule} from '@angular/material';
import { MatGridListModule} from '@angular/material';

import {
  FiltersComponent,
  PropertyTypeComponent,
  PropertyTypeDialogComponent,
  PriceComponent,
  PriceDialogComponent,
  PeopleComponent,
  PeopleDialogComponent,
} from './filters/filters.component';
import {
  MatFormFieldModule,
  MatDialogModule,
} from '@angular/material';

import { PropertyListComponent } from './property-list/property-list.component';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
 // Change this to your upload POST address:
  url: 'https://httpbin.org/post',
  maxFilesize: 50,
  acceptedFiles: 'image/*'
};

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatListModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    AppRoutingModule,
    MatStepperModule,
    MatSelectModule,
    MatIconModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatCheckboxModule,
    DropzoneModule,
    CommonModule,
    HttpClientModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatGridListModule,
    LeafletModule.forRoot()
  ],
  entryComponents: [
    PropertyTypeDialogComponent,
    PriceDialogComponent,
    PeopleDialogComponent,
    PropertyListComponent,
    PropertyListComponent
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    PropertyAddingComponent,
    PropertyListComponent,
    FiltersComponent,
    PropertyTypeComponent,
    PropertyTypeDialogComponent,
    PriceComponent,
    PriceDialogComponent,
    PeopleComponent,
    PeopleDialogComponent
  ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    },
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
