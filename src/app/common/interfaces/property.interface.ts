import {Image} from './image.interface';
import {Amenity} from './amenity.interface';

export interface Property {
    id: string;
    owner_name: string;
    owner_email: string;
    owner_phone: string;
    name: string;
    name_subtitle: string;
    city: string;
    street: string;
    postalCode: string;
    images: Image[];
    amenities: Amenity[];
    offer_description: string;
    offer_description_details: string;
    bankAccountName: string;
    bankAccountNumber: string;
    prepaymentAmount: string;
    prepaymentTerm: string;
    logo: string;
}