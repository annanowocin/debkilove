import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {Property} from '../interfaces/property.interface';

@Injectable({
  providedIn: 'root'
})
export class HttpMockService {

  properties = [];

  constructor() {
    const defaultProperties: Property[] = [
      {
        id: '1',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Karla - pokoje gościnne',
        name_subtitle: 'Pokoje 2, 3, 4 os.',
        city: 'Dębki',
        street: 'Sosnowa 4',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
      {
        id: '2',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Pensjonat Szafir',
        name_subtitle: 'Cały czerwiec 30zł/os.',
        city: 'Dębki',
        street: 'Jodłowa 9',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
      {
        id: '1',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Karla - pokoje gościnne',
        name_subtitle: 'Pokoje 2, 3, 4 os.',
        city: 'Dębki',
        street: 'Spacerowa 4',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
      {
        id: '2',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Pensjonat Szafir',
        name_subtitle: 'Cały czerwiec 30zł/os.',
        city: 'Dębki',
        street: 'Spacerowa 15',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
      {
        id: '1',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Karla - pokoje gościnne',
        name_subtitle: 'Pokoje 2, 3, 4 os.',
        city: 'Dębki',
        street: 'Liliowa 2',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
      {
        id: '2',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Pensjonat Szafir',
        name_subtitle: 'Cały czerwiec 30zł/os.',
        city: 'Dębki',
        street: 'Spacerowa 6',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
      {
        id: '1',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Karla - pokoje gościnne',
        name_subtitle: 'Pokoje 2, 3, 4 os.',
        city: 'Dębki',
        street: 'Morska 3',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
      {
        id: '2',
        owner_name: 'Anna Nowocin',
        owner_email: 'karla.debki@gmail.com',
        owner_phone: '608719017',
        name: 'Pensjonat Szafir',
        name_subtitle: 'Cały czerwiec 30zł/os.',
        city: 'Dębki',
        street: 'Spacerowa 6',
        postalCode: '84-110',
        images: [],
        amenities: [],
        offer_description: 'Millenium',
        offer_description_details: 'Millenium',
        bankAccountName: 'Millenium',
        bankAccountNumber: '23456789098765323456789234',
        prepaymentAmount: '30',
        prepaymentTerm: '5',
        logo: '5'
      },
    ];
    try {
      this.properties = JSON.parse(localStorage.getItem('properties')) || defaultProperties;
    } catch (e) {
      this.properties = defaultProperties;
      this.saveData();
    }
  }

  get(url: string) {
    let response = Observable.create(observer => {
      observer.error({status: 404});
      observer.complete();
    });

    if (url === 'properties') {
      response = Observable.create(observer => {
        observer.next(this.properties);
        observer.complete();
      });
    }
    
    if (url.indexOf('property/') !== -1) {
      response = Observable.create(observer => {
        const idToGet =  url.replace(/.*property\//, '');
        const result = this.properties.filter((item) => {
          return item.id === idToGet;
        });
        if (result[0]) {
          observer.next(result[0]);
        } else {
          observer.error({status: 404});
        }
        observer.complete();
      });
    }
    return response;
  }

  post(url: string, data: any) {
    return Observable.create(observer => {
      if (url === 'properties') {
        this.properties.push(data);
        this.saveData();
      }
      observer.next();
      observer.complete();
    });
  }

  delete(url: string) {
    return Observable.create(observer => {
      if (url.indexOf('property/') !== -1) {
        const idToRemove =  url.replace(/.*property\//, '');
        this.properties = this.properties.filter((item) => {
          return item.id !== idToRemove;
        });
        this.saveData();
      }
      observer.next();
      observer.complete();
    });
  }

  private saveData() {
    localStorage.setItem('properties', JSON.stringify(this.properties));
  }
}
