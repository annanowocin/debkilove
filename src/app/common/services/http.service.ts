import {Injectable} from '@angular/core';
// import {HttpClient} from '@angular/common/http';
import {Config} from '../../../config';
import {HttpMockService} from './http-mock.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  // change HttpMockService to HttpClient for regular api
  constructor(private http: HttpMockService) {
  }

  get(url: string) {
    return this.http.get(`${Config.API_URL}${url}`);
  }

  post(url: string, data: any) {
    return this.http.post(Config.API_URL + url, data);
  }

  //put(url: string, data: any) {
  //  return this.http.put(Config.API_URL + url, data);
  //}

}
