import { Routes } from '@angular/router';

import { PropertyAddingComponent } from '../property-adding/property-adding.component';
import { FiltersComponent } from '../filters/filters.component';

export const routes: Routes = [
  { path: 'user',  component: PropertyAddingComponent },
  { path: '**', component: FiltersComponent}
];