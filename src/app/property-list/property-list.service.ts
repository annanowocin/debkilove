import {Injectable} from '@angular/core';
import {HttpService} from '../common/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class PropertyListService {

  constructor(private httpService: HttpService) {
  }

  getProperties() {
    return this.httpService.get('properties');
  }
}
