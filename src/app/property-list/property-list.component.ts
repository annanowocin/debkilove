import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Property } from '../common/interfaces/property.interface';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PropertyListService } from './property-list.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as L from 'leaflet';
import { OpenStreetMapSearchResult } from './open-street-mapresult.interface';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.scss']
})
export class PropertyListComponent implements OnInit {
  properties: Property[] = [];
  selectedProperty: string = null;
  mapMarkerForm: FormGroup;
  myArr = [];
  map: L.Map;
  markersArr: L.Marker[] = [];

  mapLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; OpenStreetMap contributors'
  });

  options = {
    layers: [],
    zoom: 14,
    center: L.latLng([54.8297, 18.0877275])
  };

  layers: (L.TileLayer | L.Marker)[] = [this.mapLayer];

  constructor(
    private http: HttpClient,
    private propertiesListService: PropertyListService,
    private router: Router,
    private route: ActivatedRoute,
    private _formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    if (this.route.snapshot.params.id) {
      this.selectedProperty = this.route.snapshot.params.id;
    }
    this.refreshList();

    this.mapMarkerForm = this._formBuilder.group({
      addr: ['Ostrowo, żniwna 6']
    });

    for (const property of this.properties) {
      this.http
        .get(
          'https://nominatim.openstreetmap.org/search?format=json&limit=3&q=' +
            property.postalCode +
            ' ' +
            property.city +
            ' ' +
            property.street
        )
        .subscribe((data: OpenStreetMapSearchResult[]) => {
          this.myArr = data;
          for (const item of data) {
            if (item.class !== 'highway') {
              this.addMarker(item, property);
            }
          }
        });
    }
  }

  addMarker(item: OpenStreetMapSearchResult, property: Property) {
    const marker = L.marker([item.lat, item.lon], {
      icon: L.icon({
        iconSize: [25, 41],
        iconAnchor: [13, 41],
        iconUrl: 'leaflet/marker-icon.png',
        shadowUrl: 'leaflet/marker-shadow.png'
      })
    });
    marker.bindPopup(`${property.name}<br>${item.display_name}<br>Lat: ${item.lat}<br>Lon: ${item.lon}`);
    this.layers.push(marker);
  }

  refreshList() {
    this.propertiesListService.getProperties().subscribe((data: Property[]) => {
      this.properties = data;
    });
  }

  openProperty(id: string) {
    this.selectedProperty = id;
    this.router.navigate(['property', id]);
  }

  // chooseAddr(item) {
  //   this.myMarker.closePopup();
  //   this.map.setView([item.lat, item.lon], 18);
  //   this.myMarker.setLatLng([item.lat, item.lon]);
  //   this.mapMarkerForm.value.lat = item.lat;
  //   this.mapMarkerForm.value.lon =  item.lon;
  //   this.myMarker.bindPopup('Lat ' + item.lat + '<br />Lon ' + item.lon).openPopup();
  // }
}
