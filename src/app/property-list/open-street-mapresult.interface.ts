export interface OpenStreetMapSearchResult {
    boundingbox: number[];
    class: string;
    display_name: string;
    importance: number;
    lat: number;
    licence: string;
    lon: number;
    osm_id: string;
    osm_type: string;
    place_id: string;
    type: string;
};